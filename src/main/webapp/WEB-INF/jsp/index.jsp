<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
   <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <title>Listar Datillos</title>
    </head>

    <body>
        <div class="container mt-4">
            <div class="card border-info">
                <div class="card-header bg-info text-white">
                    <a href="agregar.htm" class="btn">Agregar Persona</a>
                </div>
                <div class="card-body">
                    <table border="1">
                        <thead
                            <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Apellido</th>
                        <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody
                            <c:forEach var="dato" items="${lista}">  
                                <tr>

                            <td>${dato.id}</td>
                            <td>${dato.nombre}</td>
                            <td>${dato.apellido}</td>
                            <td><a href="editar.htm?id=${dato.id}"> Editar</a> / <a href="borrar.htm?id=${dato.id}">Borrar </a></td>
                            </tr>
                        </c:forEach>                        
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </body>
</html>