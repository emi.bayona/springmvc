/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Config.Conexion;
import Models.Persona;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.xml.ws.spi.http.HttpExchange;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;



/**
 *
 * @author emiib
 */
@Controller
public class Controlador {
        
    Conexion con = new Conexion();
    @Autowired 
    private JdbcTemplate jdbcTemplate = new JdbcTemplate(con.Conectar());
    ModelAndView model = new ModelAndView();
    List datos;
    int id;
    
       @RequestMapping("index.htm")
       public ModelAndView listar(){
           String sql = "select * from persona";
           datos = this.jdbcTemplate.queryForList(sql);
           model.addObject("lista",datos);
           model.setViewName("index");
           return model;
       }
       @RequestMapping(value = "agregar.htm", method = RequestMethod.GET)
       public ModelAndView Agregar(){
           model.addObject(new Persona());
           model.setViewName("agregar");
           return model;
       }
       
        @RequestMapping(value = "agregar.htm", method = RequestMethod.POST)
       public ModelAndView Agregar(Persona p){
           String sql = "insert into persona (nombre,apellido) values (?,?)";
           this.jdbcTemplate.update(sql,p.getNombre(),p.getApellido());
           return new ModelAndView("redirect:/index.htm");
       }
       
        @RequestMapping(value = "editar.htm", method = RequestMethod.GET)
       public ModelAndView Editar(HttpServletRequest request){
          id = Integer.parseInt(request.getParameter("id"));
           String sql = "select * from persona where id="+id;
           datos = this.jdbcTemplate.queryForList(sql);
           model.addObject("lista",datos);
           model.setViewName("editar");
           return model;
       }
       
         @RequestMapping(value = "editar.htm", method = RequestMethod.POST)
       public ModelAndView Editar(Persona p){
           String sql = "update persona set nombre=?, apellido=? where id="+id;
           this.jdbcTemplate.update(sql,p.getNombre(),p.getApellido());
           return new ModelAndView("redirect:/index.htm");
       }
       
       @RequestMapping(value = "borrar.htm")
       public ModelAndView Borrar (HttpServletRequest request){
           id=Integer.parseInt(request.getParameter("id"));
           String sql = "delete from persona where id="+id;
           this.jdbcTemplate.update(sql);
           return new ModelAndView("redirect:/index.htm");
       }
  
}
